import { AppDispatch } from "../store";
import axios from "axios";
import { IUser } from "../../models/IUser";
import { userSlice } from "./UserSlice";

export const fetchUsers = () => async (dispatch: AppDispatch) => {
  try {
    dispatch(userSlice.actions.usersFetching());
    const response = await axios.get<IUser[]>("https://fakestoreapi.com/users");
    dispatch(userSlice.actions.usersFetchingSuccess(response.data));
  } catch (e) {
    // dispatch(userSlice.actions.usersFetchingError(e.massage));
  }
};
